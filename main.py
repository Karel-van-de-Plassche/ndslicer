import pandas as pd
import numpy as np
import os
import time
from collections import OrderedDict
from math import ceil
from itertools import repeat, product
import pprint

from IPython import embed
from bokeh.plotting import figure, show, reset_output, Figure
from bokeh.layouts import row, column, layout, gridplot, Spacer, widgetbox
from bokeh.models.widgets import Button, Div
from bokeh.models import HoverTool, Band
from bokeh.io import curdoc
from bokeh.models import ColumnDataSource, CustomJS, Legend, Line, Circle
from bokeh.palettes import Set1 as sepcolor
from bokeh.palettes import Plasma256
#TODO: Add sane checking if loading failed and why
import sys

var = 'efiITG_GB'

store = pd.HDFStore('./jet_base_norot_reduced_sanity_filtered_div.h5')
inp = store['/input']
plotvar = store['/output/' + var]
#xvar =
#os.chdir(os.path.dirname(os.path.realpath(__file__)))
bounds = {
    'Ane': (-2, 40),
    'x': (0, 1),
    'Ate': (-5, 40),
    'Autor': (-200, 160),
    'q': (0, 7),
    'Machtor': (-1, 55),
    'alpha': (0, 1),
    'Zeff': (1, 3.5),
    'smag': (-0.5, 5),
    'Ani0': (-2, 30),
    'Ani1': (-2, 30),
    'normni0': (0, 1),
    'normni1': (0, 0.2),
    'Ati0': (-5, 40),
    'Ti_Te0': (0.8, 1.2),
    'logNustar': (-1.5, 3),
}
show_outbound = False
def get_edges(var, lbound, rbound):
    #bins = np.linspace(lbound, rbound, 30).tolist()
    step = (bounds[var.name][1] - bounds[var.name][0]) / 100
    bins = np.arange(lbound, rbound, step).tolist()
    if show_outbound:
        bins.insert(0, -np.inf)
        bins.append(np.inf)
    #hhist, hedges = np.histogram(var, bins, density=True)
    hhist, hedges = np.histogram(var, bins)
    if show_outbound:
        hedges[0] = hedges[1] - np.diff(hedges)[1]
        hedges[-1] = hedges[-2] + np.diff(hedges)[-2]

    left = hedges[:-1]
    right = hedges[1:]
    top = hhist
    return {'left': left, 'right': right, 'top': top}


def updater(attr, old, new):
    global last_updated
    if time.time() - last_updated > 0.1:
        toolbounds = {feature: (tools[feature].x_range.start, tools[feature].x_range.end) for feature in inp.columns}
        show = True
        for feature in inp.columns:
            show &= inp[feature].between(*toolbounds[feature])
            print(show.sum())
        for feature in inp.columns:
            edges = get_edges(inp.loc[show, feature], tools[feature].x_range.start, tools[feature].x_range.end)
            cdss[feature].data = edges
        last_updated = time.time()

figs = OrderedDict()
tools = OrderedDict()
ranges = OrderedDict()
cdss = OrderedDict()
from bokeh.models import ColumnDataSource, RangeTool, Range1d
from bokeh.models.glyphs import Quad
ibounds = ColumnDataSource(bounds)
import time
last_updated = time.time()
for feature in inp.columns:
    figs[feature] = fig = figure(height=100)
    edges = get_edges(inp[feature], bounds[feature][0], bounds[feature][1])
    source = cdss[feature] = ColumnDataSource(
        edges
    )
    fig.quad(bottom=0, left=edges['left'], right=edges['right'], top=edges['top'], color="white", line_color="#3A5785")
    qd = Quad(bottom=0, left='left', right='right', top='top', line_color="#3A5785")
    fig.add_glyph(source, qd)
    #fig.quad(bottom=0, left=hedges[:-1], right=hedges[1:], top=hhist, color="white", line_color="#3A5785")
    fig.xaxis.axis_label = feature

    if show_outbound:
        range1d = ranges[feature] = Range1d(edges['left'][1], edges['right'][-2])
    else:
        range1d = ranges[feature] = Range1d(edges['left'][0], edges['right'][-1])
    range1d.on_change('end', updater)
    range1d.on_change('start', updater)
    tools[feature] = range_tool = RangeTool(x_range=range1d)
    range_tool.overlay.fill_color = "navy"
    range_tool.overlay.fill_alpha = 0.2

    #fig.line('date', 'close', source=source)
    #fig.ygrid.grid_line_color = None
    fig.add_tools(range_tool)
    #if feature == inp.columns[0]:
    fig.toolbar.active_multi = range_tool

    #hist = pd.cut(inp[feature], bins, duplicates='drop').value_counts()
    #if feature == 'Ati0':
    #    embed()
grid = gridplot(list(figs.values()), ncols=2)
updater(None, None, None)
curdoc().add_root(grid)
